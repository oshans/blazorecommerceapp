﻿
//This is a more commonly used using statement
//So, we can put it in Program.cs as GLOBAL using statement
//using Microsoft.EntityFrameworkCore;/////////////////////

namespace BlazorEcommerce.Server.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        //This will be a DB table
        //By convention we use a plural name
        public DbSet<Product> Products { get; set; }

    }
}
